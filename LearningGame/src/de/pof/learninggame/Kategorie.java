package de.pof.learninggame;

import java.util.ArrayList;

public class Kategorie {
	private ArrayList<SubKategorie> kats;
	String name;
	boolean turnedOn = true;
	
	public Kategorie(ArrayList<SubKategorie> kats, String name, boolean s){
		this.kats =  kats;
		this.name = name;
		turnedOn = s;
	}

	public ArrayList<SubKategorie> getKategories() {
		return kats;
	}

	public void setKategories(ArrayList<SubKategorie> kats) {
		this.kats = kats;
	}
	
}
