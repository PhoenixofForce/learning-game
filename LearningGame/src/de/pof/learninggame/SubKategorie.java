package de.pof.learninggame;

import java.util.ArrayList;

public class SubKategorie {
	private ArrayList<Question> ques;
	String name;
	boolean turnedOn = true;
	
	public SubKategorie(ArrayList<Question> ques, String name, boolean turned){
		this.ques = ques;
		this.name = name;
		turnedOn = turned;
	}

	public ArrayList<Question> getQues() {
		return ques;
	}

	public void setQues(ArrayList<Question> ques) {
		this.ques = ques;
	}

}
