package de.pof.learninggame;

public class Question {

	private String answer;
	private String question;
	boolean taken = false;
	
	public Question(String ques, String ans){
		question = ques;
		answer = ans;
	}

	public String getAnswer() {
		return answer;
	}

	public String getQuestion() {
		return question;
	}
	
}
