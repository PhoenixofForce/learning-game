package de.pof.learninggame;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class LearnMain {

	static File kategories = new File("kategories.txt");
	static ArrayList<Kategorie> kategores = new ArrayList<>();
	
	static ArrayList<Question> questions = new ArrayList<>();
	
	static JFrame window = new JFrame();
	
	static JLabel question = new JLabel();
	static JLabel answer = new JLabel();
	static JLabel checkAns = new JLabel();
	static JLabel yourAns = new JLabel();
	static JLabel yourPoints = new JLabel("Points: ");
	
	static JComboBox<String> chooseKategorie = new JComboBox<>();
	static JComboBox<String> chooseSubKategorie = new JComboBox<>();
	
	static JCheckBox checkKat = new JCheckBox(" ");
	static JCheckBox checkSubKat = new JCheckBox(" ");
	
	static JButton addQuestion = new JButton("Add a question");
	static JButton checkAnswer = new JButton("Check");
	static JButton completeAdding = new JButton("Add");
	static JButton next = new JButton("Next");
	static JButton options = new JButton("Options");
	static JButton addKategorie = new JButton("+ Kategorie");
	static JButton completeKatAdding = new JButton("Add");
	static JButton addSubKategorie = new JButton("+ Suborie");
	static JButton completeSubKatAdding = new JButton("Add");
	
	static JTextField answerBox = new JTextField("Insert answer here");
	static JTextField insertAnswer = new JTextField("Type answer here");
	static JTextField insertQuestion = new JTextField("Type question here");
	static JTextField insertKat = new JTextField("Type name of kategorie here");
	static JTextField insertSubKat = new JTextField("Type name of SubKategorie here");
	
	static int currentQuestion;
	static int points = 0;
	static boolean editing = false;
	static boolean inOptions = false;
	static boolean editing2 = false;
	
	public static void main(String[] args0){
		loadAllQandAs();
		drawQuestion();
		
		createJFrame(window);
		addContents(window);
		
		saveAllQandAs();
	}
	
	public static void saveAllQandAs(){
		try{
			
			if(!kategories.exists()){
				kategories.createNewFile();
			}
			
			FileWriter writer = new FileWriter(kategories);
			BufferedWriter w = new BufferedWriter(writer);
			
			for(Kategorie k: kategores){
				if(k.turnedOn)w.write(k.name + "-true");
				else w.write(k.name + "-false");
				w.newLine();
				
				File kf = new File("kategories\\" + k.name);
				if(!kf.exists())kf.mkdirs();
				kf = new File("kategories\\" + k.name + "\\Subkategories.txt");
				if(!kf.exists())kf.createNewFile();
				BufferedWriter kw = new BufferedWriter(new FileWriter(kf));
				
				for(SubKategorie s: k.getKategories()){
					if(s.turnedOn)kw.write(s.name + "-true");
					else kw.write(s.name + "-false");
					kw.newLine();
					
					File questions = new File("kategories\\" + k.name + "\\" + s.name);
					if(!questions.exists())questions.mkdirs();
					
					questions = new File("kategories\\" + k.name + "\\" + s.name + "\\questions.txt");
					File answers = new File("kategories\\" + k.name + "\\" + s.name + "\\answers.txt");
					if(!answers.exists())answers.createNewFile();
					
					BufferedWriter qw = new BufferedWriter(new FileWriter(questions));
					BufferedWriter aw = new BufferedWriter(new FileWriter(answers));
					
					for(Question q: s.getQues()){
						qw.write(q.getQuestion());
						aw.write(q.getAnswer());
						aw.newLine();
						qw.newLine();
					}
					aw.close();
					qw.close();
				}
				kw.close();
			}
			
			w.close();
		}catch(Exception e){
			e.printStackTrace();
		}

		
		
	}

	public static void loadAllQandAs(){
		if(!kategories.exists()){
			try {
				kategories.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			FileReader reader = new FileReader(kategories);
			BufferedReader r = new BufferedReader(reader);
			
			ArrayList<String> kats = new ArrayList<>();
			
			String line = r.readLine();
			
			while(line != null){
				String[] parts = line.split("-");
				kats.add(parts[0]);
				if(parts[1].equals("true"))kategores.add(new Kategorie(new ArrayList<SubKategorie>(), parts[0], true));
				else kategores.add(new Kategorie(new ArrayList<SubKategorie>(), parts[0], false));
				line = r.readLine();
			}
			
			for(int i = 0; i < kats.size(); i++){
				r = new BufferedReader(new FileReader(new File("kategories\\" + kats.get(i)+"\\Subkategories.txt")));
				
				ArrayList<String> subKats = new ArrayList<>();
				String subLine = r.readLine();
				while(subLine != null){
					String[] parts = subLine.split("-");
					subKats.add(parts[0]);
					if(parts[1].equals("true"))kategores.get(i).getKategories().add(new SubKategorie(new ArrayList<Question>(), parts[0], true));
					else kategores.get(i).getKategories().add(new SubKategorie(new ArrayList<Question>(), parts[0], false));
					subLine = r.readLine();
				}
				
				for(int x = 0; x < subKats.size(); x++){
					r = new BufferedReader(new FileReader(new File("kategories\\" + kats.get(i)+"\\" + subKats.get(x) + "\\answers.txt")));
					BufferedReader r2= new BufferedReader(new FileReader(new File("kategories\\" + kats.get(i)+"\\" + subKats.get(x) + "\\questions.txt")));
					
					String ans = r.readLine();
					String ques = r2.readLine();
					while(ans != null && ques != null){
						kategores.get(i).getKategories().get(x).getQues().add(new Question(ques, ans));
						ans = r.readLine();
						ques = r2.readLine();
					}
					r2.close();
				}
				
			}
			
			r.close();
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] katNames = new String[kategores.size()];
		for(int i = 0; i < kategores.size(); i++){
			katNames[i] = kategores.get(i).name;
		}
		chooseKategorie = new JComboBox<>(katNames);
		loadQuestions();
	}
	
	public static void loadQuestions(){
		questions = new ArrayList<>();
		for(Kategorie k: kategores){
			if(k.turnedOn){
				for(SubKategorie s: k.getKategories()){
					if(s.turnedOn){
						for(Question q: s.getQues()){
							questions.add(q);
						}
					}
				}
			}
		}
		
	}
	
	public static void drawQuestion(){
		if(questions.size() > 0){
			boolean allTaken = true;
			for(Question q: questions){
				if(!q.taken)allTaken = false;
			}
			if(allTaken){
				for(Question q: questions){
					q.taken = false;
				}
			}
			
			Random r = new Random();
			do{
				currentQuestion = r.nextInt(questions.size());
			}while(questions.get(currentQuestion).taken);
			
			question.setText(questions.get(currentQuestion).getQuestion());
			questions.get(currentQuestion).taken = true;
		}else{
			question.setText("No questions found");
		}
	}
	
	public static void createJFrame(JFrame frame){
		frame.setTitle("Learning Game");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);
		frame.setResizable(false);
		final Insets i = frame.getInsets();
		frame.setSize(500 + i.right + i.left, 250 + i.top + i.bottom);
	}
	
	public static void addContents(final JFrame frame){
		
		frame.add(insertAnswer);		
		frame.add(insertQuestion);
		frame.add(answer);
		frame.add(checkAns);
		frame.add(yourAns);
		
		frame.add(yourPoints);
		yourPoints.setText("Points: " + points);
		yourPoints.setBounds(450, 10, 125, 75);
		
		frame.add(answerBox);
		answerBox.setBounds(10, 100, 450, 75);
		
		frame.add(question);
		question.setBounds(10, 10, 450, 75);
		
		frame.add(chooseKategorie);
		frame.add(chooseSubKategorie);
		chooseKategorie.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!editing){
					if(inOptions)editing2 = true;
					String katName = (String) chooseKategorie.getSelectedItem();
					int choosen = 0;
					for(int i = 0; i < kategores.size(); i++){
						if(katName.equals(kategores.get(i).name)){
							choosen = i;
							break;
						}
					}
					
					String[] subNames =  new String[kategores.get(choosen).getKategories().size()];
					for(int i = 0; i < kategores.get(choosen).getKategories().size(); i++){
						subNames[i] = kategores.get(choosen).getKategories().get(i).name;
					}
					
					DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) chooseSubKategorie.getModel();
			        // removing old data
			        model.removeAllElements();
	
			        for (String item : subNames) {
			            model.addElement(item);
			        }
	
			        // setting model with new data
			        chooseSubKategorie.setModel(model);
			        if(inOptions)editing2 = false;
				}
				if(inOptions){
					String katName = (String) chooseKategorie.getSelectedItem();
					int choosen = 0;
					for(int i = 0; i < kategores.size(); i++){
						if(katName.equals(kategores.get(i).name)){
							choosen = i;
							break;
						}
					}
					
					checkKat.setText((String) chooseKategorie.getSelectedItem());
					checkKat.setSelected(kategores.get(choosen).turnedOn);
					checkSubKat.setText(" ");
				}
				
			}
		});
		
		chooseSubKategorie.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(inOptions && !editing2){
					
					String katName = (String) chooseSubKategorie.getSelectedItem();
					int choosen = 0;
					int choosenS = 0;
					for(int i = 0; i < kategores.size(); i++){
						for(int x = 0; x < kategores.get(i).getKategories().size(); x++){
							if(katName.equals(kategores.get(i).getKategories().get(x).name)){
								choosen = i;
								choosenS = x;
								break;
							}
						}
					}
					
					checkSubKat.setText((String) chooseSubKategorie.getSelectedItem());
					checkSubKat.setSelected(kategores.get(choosen).getKategories().get(choosenS).turnedOn);
				}
				
			}
			
		});
		
		frame.add(insertKat);
		frame.add(insertSubKat);
		frame.add(checkKat);
		checkKat.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!checkKat.getText().equals(" ")){
					String katName = (String) checkKat.getText();
					int choosen = 0;
					for(int i = 0; i < kategores.size(); i++){
						if(katName.equals(kategores.get(i).name)){
							choosen = i;
							break;
						}
					}
					kategores.get(choosen).turnedOn = checkKat.isSelected();
				}
				saveAllQandAs();
			}
			
		});
		
		frame.add(checkSubKat);
		checkSubKat.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!checkSubKat.getText().equals(" ") && !checkKat.getText().equals(" ")){
					String katName = (String) checkKat.getText();
					int choosen = 0;
					for(int i = 0; i < kategores.size(); i++){
						if(katName.equals(kategores.get(i).name)){
							choosen = i;
							break;
						}
					}
					
					String katSName = (String) checkSubKat.getText();
					int choosenS = 0;
					for(int i = 0; i < kategores.get(choosen).getKategories().size(); i++){
						if(katSName.equals(kategores.get(choosen).getKategories().get(i).name)){
							choosenS = i;
							break;
						}
					}
					
					kategores.get(choosen).getKategories().get(choosenS).turnedOn = checkSubKat.isSelected();
				}
				saveAllQandAs();
			}
			
		});
		
		//JButton
		
		frame.add(options);
		options.setBounds(10, 250-50, 100, 50);
		options.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!inOptions){
					inOptions = !inOptions;
					answerBox.setBounds(0, 0, 0, 0);
					question.setBounds(0, 0, 0, 0);
					checkAnswer.setBounds(0, 0, 0, 0);
					addQuestion.setBounds(0, 0, 0, 0);
					
					chooseKategorie.setBounds(10, 10, 100, 50);
					chooseSubKategorie.setBounds(110, 10, 100, 50);
					checkKat.setBounds(10, 70, 100, 70);
					checkSubKat.setBounds(110, 70, 100, 70);
				}else{
					inOptions = !inOptions;
					chooseKategorie.setBounds(0, 0, 0, 0);
					chooseSubKategorie.setBounds(0, 0, 0, 0);
					checkKat.setBounds(0, 0, 0, 0);
					checkKat.setText(" ");
					checkSubKat.setBounds(0, 0, 0, 0);
					checkSubKat.setText(" ");
					
					loadQuestions();
					drawQuestion();
					
					checkAnswer.setBounds(500-210, 250-50, 100, 50);
					answerBox.setBounds(10, 100, 450, 75);
					question.setBounds(10, 10, 450, 75);
					addQuestion.setBounds(500-100, 250-50, 100, 50);
				}
			}
		});
		
		frame.add(checkAnswer);
		checkAnswer.setBounds(500-210, 250-50, 100, 50);
		checkAnswer.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				answerBox.setBounds(0, 0, 0, 0);
				question.setBounds(0, 0, 0, 0);
				answer.setBounds(0, 0, 0, 0);
				checkAnswer.setBounds(0, 0, 0, 0);
				options.setBounds(0, 0, 0, 0);
				
				next.setBounds(500-210, 250-50, 100, 50);
				checkAns.setBounds(10, 10, 490, 75);
				yourAns.setBounds(10, 100, 490, 75);
				
				String typedAnswer = answerBox.getText();
				String rightAnswer = questions.get(currentQuestion).getAnswer();
				
				if(rightAnswer.equalsIgnoreCase(typedAnswer)){
					System.out.println("The correct answer was there");
					points += 3;
					yourPoints.setText("Points: " + points);
				}else if(rightAnswer.contains(typedAnswer)){
					System.out.println("The correct answer was there");
					points += 1;
					yourPoints.setText("Points: " + points);
				}else{
					System.out.println("The correct answer was not there");
					points = Math.max(points - 1, 0);
					yourPoints.setText("Points: " + points);
				}
				
				checkAns.setText("Right answer: " + rightAnswer);
				yourAns.setText("You typed: " + typedAnswer);
				
			}
		});
	
		frame.add(next);
		next.setBounds(0, 0, 0, 0);
		next.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				drawQuestion();
				yourAns.setBounds(0, 0, 0, 0);
				checkAns.setBounds(0, 0, 0, 0);
				next.setBounds(0, 0, 0, 0);
				
				answerBox.setText("Insert answer here");
				
				answerBox.setBounds(10, 100, 490, 75);
				question.setBounds(10, 10, 490, 75);
				options.setBounds(10, 250-50, 100, 50);
				checkAnswer.setBounds(500-210, 250-50, 100, 50);
			}
			
		});
		
		frame.add(completeAdding);
		completeAdding.setBounds(0, 0, 0, 0);
		completeAdding.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				insertQuestion.setBounds(0, 0, 0, 0);
				insertAnswer.setBounds(0, 0, 0, 0);
				completeAdding.setBounds(0, 0, 0, 0);
				chooseKategorie.setBounds(0, 0, 0, 0);
				chooseSubKategorie.setBounds(0, 0, 0, 0);
				addKategorie.setBounds(0, 0, 0, 0);
				addSubKategorie.setBounds(0, 0, 0, 0);
				
				addQuestion.setBounds(500-100, 250-50, 100, 50);
				options.setBounds(10, 250-50, 100, 50);
				answerBox.setBounds(10, 100, 490, 75);
				question.setBounds(10, 10, 490, 75);
				checkAnswer.setBounds(500-210, 250-50, 100, 50);
				
				if(!insertAnswer.getText().equals("Type answer here") && !insertQuestion.getText().equals("Type question here")){
					String katName = (String) chooseKategorie.getSelectedItem();
					int choosenK = 0;
					for(int i = 0; i < kategores.size(); i++){
						if(katName.equals(kategores.get(i).name)){
							choosenK = i;
							break;
						}
					}
					
					String subName = (String) chooseSubKategorie.getSelectedItem();
					int choosenS = 0;
					for(int i = 0; i < kategores.get(choosenK).getKategories().size(); i++){
						if(subName.equals(kategores.get(choosenK).getKategories().get(i).name)){
							choosenS = i;
							break;
						}
					}
					kategores.get(choosenK).getKategories().get(choosenS).getQues().add(new Question(insertQuestion.getText(), insertAnswer.getText()));
					
				}
				
				insertQuestion.setText("Type question here");
				insertAnswer.setText("Type answer here");
				saveAllQandAs();
				
				for(Question q: questions){
					q.taken = false;
				}
				loadQuestions();
				drawQuestion();
			}
		});
		
		frame.add(addKategorie);
		addKategorie.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				addSubKategorie.setBounds(0, 0, 0, 0);
				addKategorie.setBounds(0, 0, 0, 0);
				chooseKategorie.setBounds(0, 0, 0, 0);
				chooseSubKategorie.setBounds(0, 0, 0, 0);
				insertQuestion.setBounds(0, 0, 0, 0);
				options.setBounds(0, 0, 0, 0);
				insertAnswer.setBounds(0, 0, 0, 0);
				completeAdding.setBounds(0, 0, 0, 0);
				
				insertKat.setBounds(10, 10, 490, 75);
				completeKatAdding.setBounds(500-75, 250-50, 75, 50);
			}
		});
		
		frame.add(completeKatAdding);
		completeKatAdding.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				editing = true;
				if(!insertKat.getText().equals("Type name of kategorie here")){
					kategores.add(new Kategorie(new ArrayList<SubKategorie>(), insertKat.getText(), true));
				}
				
				String[] subNames =  new String[kategores.size()];
				for(int i = 0; i < kategores.size(); i++){
					subNames[i] = kategores.get(i).name;
				}
				
				DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) chooseKategorie.getModel();
		        model.removeAllElements();

		        for (String item : subNames) {
		            model.addElement(item);
		        }

		        chooseKategorie.setModel(model);
		        insertKat.setText("Type name of kategorie here");
		        editing = false;
		        
		        saveAllQandAs();
		        
				answerBox.setBounds(0, 0, 0, 0);
				question.setBounds(0, 0, 0, 0);
				answer.setBounds(0, 0, 0, 0);
				checkAnswer.setBounds(0, 0, 0, 0);
				addQuestion.setBounds(0, 0, 0, 0);
				completeKatAdding.setBounds(0, 0, 0, 0);
				insertKat.setBounds(0, 0, 0, 0);
				
				addSubKategorie.setBounds(320, 225, 100, 25);
				addKategorie.setBounds(320, 200, 100, 25);
				chooseKategorie.setBounds(120, 200, 100, 50);
				chooseSubKategorie.setBounds(220, 200, 100, 50);
				insertQuestion.setBounds(10, 10, 490, 75);
				insertAnswer.setBounds(10, 100, 490, 75);
				completeAdding.setBounds(500-75, 250-50, 75, 50);
				}
			
		});
	
		frame.add(completeSubKatAdding);
		completeSubKatAdding.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				chooseKategorie.setBounds(0, 0, 0, 0);
				insertSubKat.setBounds(0, 0, 0, 0);
				completeSubKatAdding.setBounds(0, 0, 0, 0);
				
				String katName = (String) chooseKategorie.getSelectedItem();
				int choosen = 0;
				for(int i = 0; i < kategores.size(); i++){
					if(katName.equals(kategores.get(i).name)){
						choosen = i;
						break;
					}
				}
				
				if(!insertSubKat.getText().equals("Type name of SubKategorie here")){
					kategores.get(choosen).getKategories().add(new SubKategorie(new ArrayList<Question>(), insertSubKat.getText(), true));
				}
				
				String[] subNames =  new String[kategores.get(choosen).getKategories().size()];
				for(int i = 0; i < kategores.get(choosen).getKategories().size(); i++){
					subNames[i] = kategores.get(choosen).getKategories().get(i).name;
				}
				
				DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) chooseSubKategorie.getModel();
		        model.removeAllElements();

		        for (String item : subNames) {
		            model.addElement(item);
		        }
		        chooseSubKategorie.setModel(model);
				
		        insertSubKat.setText("Type name of SubKategorie here");
		        
				editing = false;
				addSubKategorie.setBounds(320, 225, 100, 25);
				addKategorie.setBounds(320, 200, 100, 25);
				chooseKategorie.setBounds(120, 200, 100, 50);
				chooseSubKategorie.setBounds(220, 200, 100, 50);
				insertQuestion.setBounds(10, 10, 490, 75);
				insertAnswer.setBounds(10, 100, 490, 75);
				completeAdding.setBounds(500-75, 250-50, 75, 50);
			}
			
		});
		
		frame.add(addSubKategorie);
		addSubKategorie.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				editing = true;
				addSubKategorie.setBounds(0, 0, 0, 0);
				options.setBounds(0, 0, 0, 0);
				addKategorie.setBounds(0, 0, 0, 0);
				chooseKategorie.setBounds(0, 0, 0, 0);
				chooseSubKategorie.setBounds(0, 0, 0, 0);
				insertQuestion.setBounds(0, 0, 0, 0);
				insertAnswer.setBounds(0, 0, 0, 0);
				completeAdding.setBounds(0, 0, 0, 0);
				
				chooseKategorie.setBounds(10, 100, 100, 50);
				insertSubKat.setBounds(10, 10, 490, 75);
				completeSubKatAdding.setBounds(500-75, 250-50, 75, 50);
			}
			
		});

		frame.add(addQuestion);
		addQuestion.setBounds(500-100, 250-50, 100, 50);
		addQuestion.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				answerBox.setBounds(0, 0, 0, 0);
				question.setBounds(0, 0, 0, 0);
				answer.setBounds(0, 0, 0, 0);
				checkAnswer.setBounds(0, 0, 0, 0);
				options.setBounds(0, 0, 0, 0);
				addQuestion.setBounds(0, 0, 0, 0);
				
				addSubKategorie.setBounds(320, 225, 100, 25);
				addKategorie.setBounds(320, 200, 100, 25);
				chooseKategorie.setBounds(120, 200, 100, 50);
				chooseSubKategorie.setBounds(220, 200, 100, 50);
				insertQuestion.setBounds(10, 10, 490, 75);
				insertAnswer.setBounds(10, 100, 490, 75);
				completeAdding.setBounds(500-75, 250-50, 75, 50);
				
			}
		});
	}
}